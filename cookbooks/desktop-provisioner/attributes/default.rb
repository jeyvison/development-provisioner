default['desktop-provisioner']['temp-path'] = 'C:/temp'
default['desktop-provisioner']['eclipse-download-path'] = default['desktop-provisioner']['temp-path'] + '/eclipse.zip'
default['desktop-provisioner']['eclipse-download-url'] = 'http://mirror.nbtelecom.com.br/eclipse/technology/epp/downloads/release/luna/SR2/eclipse-java-luna-SR2-win32-x86_64.zip'
default['desktop-provisioner']['eclipse-home'] = 'C:/temp/eclipse'
