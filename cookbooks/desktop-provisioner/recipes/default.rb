#
# Cookbook Name:: desktop-provisioner
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'chocolatey'

eclipse_home=node['desktop-provisioner']['eclipse-home']
eclipse_download_url=node['desktop-provisioner']['eclipse-download-url']
eclipse_download_path=node['desktop-provisioner']['eclipse-download-path']

if !Dir.exist?(node['desktop-provisioner']['temp-path'])
	Chef::Log.info('Temporary path does not exist. Creating...')
	directory node['desktop-provisioner']['temp-path'] do
		action :create
	end
else
	Chef::Log.info('Temp Directory already exists. Skipping creation...')
end


remote_file "#{eclipse_download_path}" do
	source "#{eclipse_download_url}"
	action :create_if_missing
end


chocolatey 'git.install' do
	version '2.6.4'
	options({ 'params' => "'/GitAndUnixToolsOnPath'"})
	not_if 'git --version'
end


chocolatey 'jdk7' do
	action :install
	not_if 'javac -version'
end

windows_zipfile "#{eclipse_home}"  do
	source  "#{eclipse_download_path}"
	action :unzip
	not_if {::Dir.exist?("#{eclipse_home}")}
end
